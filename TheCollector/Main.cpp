#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <string>
#include <vector>
#include "Item.h"
#include <cstdlib>
#include <time.h>
#include "Player.h"

int main()
{
	sf::RenderWindow gameWindow;
	gameWindow.create(sf::VideoMode::getDesktopMode(), "The Collector", sf::Style::Titlebar | sf::Style::Close);

	// -----------------------------------------------
	// Game Setup
	// -----------------------------------------------
	// Player
// Declare a texture variable called playerTexture
	sf::Texture playerTexture;
	// Load up our texture from a file path
	playerTexture.loadFromFile("Assets/Graphics/player.png");
	// Declare a player object
	Player playerObject(playerTexture, gameWindow.getSize());


	//Music
	sf::Music gameMusic;
	gameMusic.openFromFile("Assets/Audio/music.ogg");
	gameMusic.play();

	//Font
	sf::Font gameFont;
	gameFont.loadFromFile("Assets/Fonts/mainFont.ttf");

	sf::Text titleText;
	titleText.setFont(gameFont);
	titleText.setString("The Collector");
	titleText.setCharacterSize(24);
	titleText.setFillColor(sf::Color::Cyan);
	titleText.setStyle(sf::Text::Bold | sf::Text::Italic);
	titleText.setPosition(gameWindow.getSize().x / 2 - titleText.getLocalBounds().width / 2, 30);

	//Score
	int score = 0;
	sf::Text scoreText;
	scoreText.setFont(gameFont);
	scoreText.setString("Score: " + std::to_string(score));
	scoreText.setCharacterSize(16);
	scoreText.setFillColor(sf::Color::White);
	scoreText.setPosition(30, 30);

	//Time
	sf::Text timerText;
	timerText.setFont(gameFont);
	timerText.setString("Time Remaining: 0");
	timerText.setCharacterSize(16);
	timerText.setFillColor(sf::Color::White);
	timerText.setPosition(gameWindow.getSize().x - timerText.getLocalBounds().width - 30, 30);
	sf::Time timeLimit = sf::seconds(60.0f);
	sf::Time timeRemaining = timeLimit;
	sf::Clock gameClock;

	//Random
	srand(time(NULL));


	// Items
	// Load all three textures that will be used by our items
	std::vector<sf::Texture> itemTextures;
	itemTextures.push_back(sf::Texture());
	itemTextures.push_back(sf::Texture());
	itemTextures.push_back(sf::Texture());
	itemTextures[0].loadFromFile("Assets/Graphics/coinBronze.png");
	itemTextures[1].loadFromFile("Assets/Graphics/coinSilver.png");
	itemTextures[2].loadFromFile("Assets/Graphics/coinGold.png");

	// Create the vector to hold our items
	std::vector<Item> items;
	// Load up a few starting items
	items.push_back(Item(itemTextures, gameWindow.getSize()));
	items.push_back(Item(itemTextures, gameWindow.getSize()));
	items.push_back(Item(itemTextures, gameWindow.getSize()));
	
	// Create a time value to store the total time between each itemspawn
	sf::Time itemSpawnDuration = sf::seconds(2.0f);
	// Create a timer to store the time remaining for our game
	sf::Time itemSpawnRemaining = itemSpawnDuration;

	sf::Vector2f playerVelocity(0.0f, 0.0f);
	
	float speed = 100.0f;

	// Load the pickup sound effect file into a soundBuffer
	sf::SoundBuffer pickupSoundBuffer;
	pickupSoundBuffer.loadFromFile("Assets/Audio/pickup.wav");
	// Setup a Sound object to play the sound later, and associate it with the SoundBuffer
	sf::Sound pickupSound;
	pickupSound.setBuffer(pickupSoundBuffer);

	// Load the victory sound effect file into a soundBuffer
	sf::SoundBuffer victorySoundBuffer;
	victorySoundBuffer.loadFromFile("Assets/Audio/victory.ogg");
	// Setup a Sound object to play the sound later, and associate it with the SoundBuffer
	sf::Sound victorySound;
	victorySound.setBuffer(victorySoundBuffer);

	// Game over variable to track if the game is done
	bool gameOver = false;

	// Game Over Text
// Declare a text variable called gameOverText to hold our game over display
		sf::Text gameOverText;
	// Set the font our text should use
	gameOverText.setFont(gameFont);
	// Set the string of text that will be displayed by this text object
	gameOverText.setString("GAME OVER\nPrees R to restart\nPress Q to quit");
	// Set the size of our text, in pixels
	gameOverText.setCharacterSize(72);
	// Set the colour of our text
	gameOverText.setFillColor(sf::Color::Cyan);
	// Set the text style for the text
	gameOverText.setStyle(sf::Text::Bold | sf::Text::Italic);
	// Position our text in the top center of the screen
	gameOverText.setPosition(gameWindow.getSize().x / 2 - gameOverText.getLocalBounds().width / 2, 150);


	// Game Loop
	while (gameWindow.isOpen())
	{
		// -----------------------------------------------
		// Input Section
		// -----------------------------------------------
		sf::Event gameEvent;
		while (gameWindow.pollEvent(gameEvent))
		{
			// This section will be repeated for each event waiting to be processed
		
			if (gameEvent.type == sf::Event::Closed)
			{
				gameWindow.close();
			}
		}

		// Player keybind input
		playerObject.Input();

		// Check if we should reset the game
		if (gameOver)
		{
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::R))
			{

				// Reset the game
				score = 0;
				timeRemaining = timeLimit;
				items.clear();
				items.push_back(Item(itemTextures, gameWindow.getSize()));
				items.push_back(Item(itemTextures, gameWindow.getSize()));
				items.push_back(Item(itemTextures, gameWindow.getSize()));
				gameMusic.play();
				gameOver = false;
				playerObject.Reset(gameWindow.getSize());
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Q))
			{
				gameWindow.close();
			}
		}

		// -----------------------------------------------
		// Update Section
		// -----------------------------------------------

		//Update Timer
		sf::Time frameTime = gameClock.restart();
		timeRemaining = timeRemaining - frameTime;

		// Check if time has run out
		if (timeRemaining.asSeconds() <= 0)
		{
			// Don't let the time go lower than 0
			timeRemaining = sf::seconds(0);
			// Perform these actions only once when the game first ends:
			if (gameOver == false)
			{
				// Set our gameOver to true now so we don't perform these actions again
					gameOver = true;
				// Stop the main music from playing
				gameMusic.stop();
				// Play our victory sound
				victorySound.play();
			}
		}

		timerText.setString("Time Remaining: " + std::to_string((int)timeRemaining.asSeconds()));
		// Only perform this update logic if the game is still running:
		if (!gameOver)
		{

			// Update our item spawn time remaining based on how much time passed last frame
			itemSpawnRemaining = itemSpawnRemaining - frameTime;
			// Check if time remaining to next spawn has reached 0
			if (itemSpawnRemaining <= sf::seconds(0.0f))
			{
				// Time to spawn a new item!
				items.push_back(Item(itemTextures, gameWindow.getSize()));
				// Reset time remaining to full duration
				itemSpawnRemaining = itemSpawnDuration;
			}

			//Update Score
			//score = score + 1;
			scoreText.setString("Score: " + std::to_string(score));

			// Move the player
			playerObject.Update(frameTime);

			// Check for collisions
			for (int i = items.size() - 1; i >= 0; --i)
			{
				sf::FloatRect itemBounds = items[i].sprite.getGlobalBounds();
				sf::FloatRect playerBounds = playerObject.sprite.getGlobalBounds();
				if (itemBounds.intersects(playerBounds))
				{
					// Our player touched the item!
					// Add the item's value to the score
					score += items[i].pointsValue;
					// Remove the item from the vector
					items.erase(items.begin() + i);
					// Play the pickup sound
					pickupSound.play();

				}
			}
		}
		// -----------------------------------------------
		// Draw Section
		// -----------------------------------------------

		// Clear the window to a single colour
		gameWindow.clear(sf::Color::Black);
		// Draw everything to the window
		gameWindow.draw(playerObject.sprite);

		//Draw Title
		gameWindow.draw(titleText);

		//Draw Score
		gameWindow.draw(scoreText);

		//Draw Timer
		gameWindow.draw(timerText);

		if (!gameOver)
		{

			gameWindow.draw(playerObject.sprite);

			// Draw all our items
			for (int i = 0; i < items.size(); ++i)
			{
				gameWindow.draw(items[i].sprite);
			}
		}

		// Only draw these items if the game HAS ended:
		if (gameOver)
		{
			gameWindow.draw(gameOverText);
		}

		// Display the window contents on the screen
		gameWindow.display();
	
	}
	// End of Game Loop
	return 0;
}